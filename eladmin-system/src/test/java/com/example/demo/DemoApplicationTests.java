package com.example.demo;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.rongdu.system.mapper.UserMybatisMapper;
import javafx.application.Application;
import me.zhengjie.AppRun;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;
import java.util.Map;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = AppRun.class)
public class DemoApplicationTests {

    @Autowired
    private UserMybatisMapper userMapper;

    @Test
    public void contextLoads() {
    }

    @Test
    public void queryUserByName() {
        Map<String,Object> user = userMapper.queryUserByName("admin11");
        Assert.assertNotNull("user不能为空",user);
        System.out.println(user.get("password"));
    }

    @Test
    public void selectAllUser() {
        PageHelper.startPage(1, 2);//1,10
        List<Map<String,Object>> user = userMapper.selectAllUser();

        PageInfo<Map<String,Object>> pageInfoUser = new PageInfo<Map<String,Object>>(user);
        Assert.assertNotNull("user不能为空",user);
        System.out.println(user.size());
        System.out.println(pageInfoUser.getTotal());
    }
}
