package com.rongdu.system.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * @Description
 * @Author wangdk, wangdk@erongdu.com
 * @CreatTime 2019/8/19 14:08
 * @Since version 1.0.0
 */
public interface UserMybatisMapper {
    @Select("SELECT * FROM user")
    List<Map<String,Object>> selectAllUser();

    Map<String,Object> queryUserByName(@Param("username") String username);
}
